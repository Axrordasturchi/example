@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
        <div class="col-md-8">
          <form action="/admin/edit/saqlash/{{$id}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">FIO</label>
              <input type="text" name="name"class="form-control" value="{{$user->name}}">
              <small id="emailHelp" class="form-text text-muted">
              Bu sizning bazadagi nomingiz</small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Email</label>
              <input type="email" name="email" class="form-control" value="{{$user->email}}">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Vazifasi</label>
              <input type="text" name="vazifasi" class="form-control" value="{{$user->vazifasi}}">
            </div>
            <div>
                <input type="file" name="img">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
      </div>
    </div>
</div>
@endsection