<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Students;
use Illuminate\Support\Facades\Session;
class AdminController extends Controller
{

	public function __construct()
    {
        $this->middleware(['auth',\App\Http\Middleware\AdminMiddileware::class]);
    }


  
     public function delete($id)
	    {
	     
	        // DB::delete('DELETE FROM users WHERE id = ? ', [$id]);
	        $students = new Students();
	        $students->WHERE ('id',$id)->delete();
	        $page = (int)\request()->get('page');
	        if ($page === 0) $page = 1;
	        return redirect('/home?page='.$page);
	    }
     public function edit($id)
    	{
     		$records = DB::table('students')->where('id',$id)->first();
     		return view('edit',['user'=>$records,'id'=>$id]);
	        
    	}
    public function saqlash($id, Request $request)
	    {

	    	$students = new Students();
	    	$request->img->storeAs('images',$id.'.jpg','public'); 
		    
		    $name=$_POST['name'];
	        $email=$_POST['email'];
	        $vazifasi=$_POST['vazifasi'];
	         $img=$id.'.jpg';
	        DB::table('students')->WHERE ('id',$id)->update(['name'=>$name,'email'=>$email,
	        	'vazifasi'=>$vazifasi, 'img'=>$img 
	        	 ]);
	        return redirect('/home');
	    }
	public function add()
	{
		return view('/add');
	}
	 public function save()
    {
        $students = new Students();
        $students->name=$_GET["name"];
        $students->email=$_GET["email"];
        $students->guruh=$_GET["guruh"];
        $students->vazifasi=$_GET["vazifasi"];
        
        $students->save();
        
        // DB::insert('insert into users (name,email) 
        // values (?,?)',["{$name}","{$email}"]);
        return redirect('/home');

    }
    public function profile()
    {
    	return view('profile');
    }
    public function saveprofile($id, Request $request)
	    {

	    	$students = new Students();
	    	$request->img->storeAs('images',$id.'.jpg','public'); 
		    
		   
	         $img=$id.'.jpg';
	        DB::table('students')->WHERE ('id',$id)->update(['img'=>$img 
	        	 ]);
	        return redirect('/profile');
	    }
}
