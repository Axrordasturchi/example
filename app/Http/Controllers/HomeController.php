<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Students;
use Illuminate\Support\Facades\Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(intval(Auth::user()->admin) === 1){
          $pe = 15;
                $students = Students::join('user_types','user_types.id','=','students.type_id','left')
                    ->select('students.*', 'user_types.name AS uTypeName');
                
        $searchText = trim(\request()->get('search'));
        /////////////////////
                    $clear =(int)\request()->get('clear');

                    if (empty($searchText))
                    {
                        if ($clear === 1)
                        {
                            Session::put('search','');
                        }
                        else 
                        {
                            $searchText = Session::get('search');
                        }

                    }


                    else {
                            Session::put('search', $searchText);
                    }

        ///////////////////////////////////

        if (!empty($searchText))
        {
            $students->where('students.name','like','%'.$searchText.'%');
        }

        else 
        {

        }

///////////////////////////
        $uType =(int)\request()->get('uType');

        if ($uType === 0)
        {
            if ($clear === 1)
            {
                Session::put('uType',0);
            }
            else 
            {
                $uType = Session::get('uType');
            }

        }
        else 
        {
            Session::put('uType', $uType);
        }

        if ($uType > 0)
        {
            $students->where('type_id','=',$uType);
        }




        $records = $students->paginate($pe);
        $page = (int)\request()->get('page');
        if ($page === 0) $page = 1;
        $start = $pe * ($page-1);

        $uTypes = DB::table('user_types')->get();

        return view('/admin',
            ['students'=>$records, 
            'start'=>$start,
            'searchText'=>$searchText,
            'uTypes'=>$uTypes,
            'uTypeId'=>$uType,
            'pageId'=>$page
        ]);
     
 }
     else{
        return view('home');
    }
    
       
}

}
